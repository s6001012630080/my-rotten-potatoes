Rails.application.routes.draw do

  devise_for :users, :controllers => { :omniauth_callbacks => "callbacks" }
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :movies
  root :to => redirect('/movies')

  post '/movies/search_tmdb'

  # get  'auth/:provider/callback' => 'sessions#create'
  # post 'logout' => 'sessions#destroy'
  # get  'auth/failure' => 'sessions#failure'
  # get  'auth/twitter', :as => 'login'

end
