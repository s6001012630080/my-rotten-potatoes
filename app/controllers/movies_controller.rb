require 'themoviedb'

class MoviesController < ApplicationController
  def index
    @movies = Movie.all
  end

  def show
    id = params[:id] # retrieve movie ID from URI route
    @movie = Movie.find(id) # look up movie by unique ID
    # will render app/views/movies/show.html.haml by default
  end

  def new
    @movie = Movie.new
    # default: render 'new' template
  end

  def create
    @movie = Movie.create!(movie_params)
    if @movie.save
      flash[:notice] = "#{@movie.title} was successfully created."
      redirect_to movie_path(@movie)
    else
      render 'new' # note, 'new' template can access @movie's field values!
    end
  end

  def edit
    @movie = Movie.find params[:id]
  end

  def update
    @movie = Movie.find params[:id]
    if @movie.update_attributes(movie_params)
      flash[:notice] = "#{@movie.title} was successfully updated."
      redirect_to movie_path(@movie)
    else
      render 'edit' # note, 'edit' template can access @movie's field values!
    end
  end

  def destroy
    @movie = Movie.find(params[:id])
    @movie.destroy
    flash[:notice] = "Movie '#{@movie.title}' deleted."
    redirect_to movies_path
  end

  # def self.all_ratings ; %w[G PG PG-13 R NC-17] ; end #  shortcut: array of strings
  # validates :title, :presence => true
  # validates :release_date, :presence => true
  # validates :released_1930_or_later # uses custom validator below
  # validates :rating, :inclusion => {:in => Movie.all_ratings},
  #           :unless => :grandfathered?
  # def released_1930_or_later
  #   errors.add(:release_date, 'must be 1930 or later') if
  #       release_date && release_date < Date.parse('1 Jan 1930')
  # end
  # @@grandfathered_date = Date.parse('1 Nov 1968')
  # def grandfathered?
  #   release_date && release_date < @@grandfathered_date
  # end

  def search_tmdb
    Tmdb::Api.key '9177536d0a09fcc038145e214b338de7'

    search = Tmdb::Search.new
    search.resource('movie')
    search.query("'#{params[:search_terms]}'")
    result = search.fetch

    if result[0].nil?
      flash[:warning] = "'#{params[:search_terms]}' was not found in TMDb"
    else
      result.each do |each|
        @movie = Movie.create!(title: each['original_title'], rating: 'G', release_date: each['release_date'])
      end
    end
    redirect_to movies_path
  end

  private

  def movie_params
    params.require(:movie).permit(:title,:rating,:release_date)
  end
end
